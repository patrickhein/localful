#!/bin/bash
set -euo pipefail

DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"

for directory in "${DIR}/pods"/*/; do
  echo ""

  pod=$(basename "$directory")
  echo deploying "$pod"

  configmap="${DIR}/configs/${pod}.yml"
  configmap_argument=""

  if [[ -f "$configmap" ]]; then
    echo "loading configmap"
    configmap_argument="--configmap $configmap"
  fi

  (cd "${DIR}/pods/${pod}" && podman kube play --replace $configmap_argument "${DIR}/pods/${pod}/pod.yml")
done
