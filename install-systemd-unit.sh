#!/bin/bash
set -euo pipefail

DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"

cp -v deploy-pods@.service "${HOME}/.config/systemd/user/deploy-pods@.service"
systemctl --user daemon-reload

name=$(systemd-escape "${DIR}/deploy.sh")
echo -e "enable with\nsystemctl --user enable --now deploy-pods@${name}.service"
